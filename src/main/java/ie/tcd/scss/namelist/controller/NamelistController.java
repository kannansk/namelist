package ie.tcd.scss.namelist.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
public class NamelistController {

    private List<String> nameList = new ArrayList<>();

    @GetMapping("/reset")
    public ResponseEntity<Void> reset() {
        nameList.clear();
        return ResponseEntity.ok().build();
    }

    @PostMapping("/names")
    public ResponseEntity<String> addName(@RequestBody NameDto nameDto) {
        String name = nameDto.getName();
        if (name == null || name.trim().isEmpty()) {
            return ResponseEntity.status(HttpStatus.FORBIDDEN).body("Empty names are not allowed.");
        }

        if (!nameList.contains(name)) {
            nameList.add(name);
            return ResponseEntity.status(HttpStatus.CREATED).body("Name added: " + name);
        } else {
            return ResponseEntity.ok("Name already exists: " + name);
        }
    }

    @GetMapping("/names")
    public ResponseEntity<String> getNames() {
        if (nameList.isEmpty()) {
            return ResponseEntity.ok("(List of names is empty)");
        } else {
            Collections.sort(nameList);
            String names = String.join(", ", nameList);
            return ResponseEntity.ok(names);
        }
    }

    public static class NameDto {
        private String name;
           
        public NameDto() {
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }
    }
}
