package ie.tcd.scss.namelist.controller;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.HashMap;
import java.util.Map;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * This test tests the NamelistController class.
 */
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class NamelistControllerTest {

    @LocalServerPort
    private int port;

    @Autowired
    private TestRestTemplate restTemplate;

    // Reset the names list before running each test
    @BeforeEach
    public void resetNamesList() {
        restTemplate.getForEntity("http://localhost:" + port + "/reset", Void.class);
    }

    @Test
    public void getNamesShouldReturnEmptyMessageIfNoNames() {
        ResponseEntity<String> response = restTemplate.getForEntity("http://localhost:" + port + "/names", String.class);
        assertThat(response.getBody()).isEqualTo("(List of names is empty)");
    }

    @Test
    public void postNamesShouldAddName() {
        Map<String, String> request = new HashMap<>();
        request.put("name", "Paul");

        ResponseEntity<Void> response = this.restTemplate.postForEntity("http://localhost:" + port + "/names", request, Void.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        ResponseEntity<String> response2 = restTemplate.getForEntity("http://localhost:" + port + "/names", String.class);
        assertThat(response2.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response2.getBody()).isEqualTo("Paul");
    }

    @Test
    public void postNamesWithSpecialCharacters() {
        Map<String, String> request = new HashMap<>();
        request.put("name", "Björk");
        ResponseEntity<Void> response = this.restTemplate.postForEntity("http://localhost:" + port + "/names", request, Void.class);
        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        Map<String, String> request2 = new HashMap<>();
        request2.put("name", "José");
        ResponseEntity<Void> response2 = this.restTemplate.postForEntity("http://localhost:" + port + "/names", request2, Void.class);
        assertThat(response2.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        Map<String, String> request3 = new HashMap<>();
        request3.put("name", "Øystein");
        ResponseEntity<Void> response3 = this.restTemplate.postForEntity("http://localhost:" + port + "/names", request3, Void.class);
        assertThat(response3.getStatusCode()).isEqualTo(HttpStatus.CREATED);

        ResponseEntity<String> response4 = restTemplate.getForEntity("http://localhost:" + port + "/names", String.class);

        // accept both variants, (1) separated by comma only and (2) separated by comma and space
        assertThat(response4.getBody()).isIn("Björk, José, Øystein", "Björk,José,Øystein");

    }

    // Some tests removed

}

